#include <iostream>
#include <string>

int main(int argc, char **argv)
{
    using namespace std;

    if (argc > 1 && string(argv[1]) == "test") {
        cout << "testing..." << endl;
    } else {
        cout << "running..." << endl;
    }

    return 0;
}
